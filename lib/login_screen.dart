import 'package:flutter/material.dart';
void main() => runApp(MaterialApp(
      home:SecondRoute(),
      debugShowCheckedModeBanner: false,
    ));

 class SecondRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:
        Container(
            width:MediaQuery.of(context).size.width,
            height:MediaQuery.of(context).size.height,
            
            alignment: Alignment.bottomRight,
            decoration: BoxDecoration(
                image: DecorationImage( 
              colorFilter: new ColorFilter.mode(Colors.redAccent[100].withOpacity(0.9), BlendMode.color),
                    image: AssetImage(
                        'assets/j.jpg'),
                   fit: BoxFit.fill 
                   
             ) ),

        child: RaisedButton(
          onPressed: () {
          
          },
          child: Text('Logout!'),
        padding: EdgeInsets.fromLTRB(10,10,10,10), 
        ),
      ),
    );
  }
}