import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
//import 'package:haxer_loginform/login_screen.dart';
//import 'package:haxer_loginform/otp_screen.dart';
//import 'package:haxer_loginform/signup_screen.dart';
//import 'package:haxer_loginform/login_screen.dart';
import 'package:haxer_loginform/Forgetpassword_screen.dart';

//import 'mainhome.dart';
import 'signup_screen.dart';
//import 'package:firebase_auth/firebase_auth.dart';

void main() => runApp(MaterialApp(
      home:new MyApp(),
      debugShowCheckedModeBanner: false,
    ));

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MyAppState();
    
  }
}
// class Responsive extends StatelessWidget{

//   Widget build (BuildContext context){
//     return LayoutBuilder(
//     builder: (context, constraints){
//       return OrientationBuilder( 
//     builder: (context,orientation){
//       SizeConfig().init(constraints,orientation);
//                   return MaterialApp(
                    
//                   );
//                 },
//               );
//                 },
//                 );
//               }
//             }

// class AppTheme {
// }
            
//             class SizeConfig {
//         void init(constraints, Orientation orientation) {}
// }
class MyAppState extends State<MyApp> {
  TextEditingController _passwordController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  // final _formKeypassword = GlobalKey<FormState>();
  // final _scaffoldKey = GlobalKey<ScaffoldState>();
bool autovalidateMode=true;
bool screenorientation=true;
  set _email(String _email){}
  set _password(String _password){}
  @override
  Widget build(BuildContext context) {
        return 
       Scaffold(
         resizeToAvoidBottomInset: false,
         resizeToAvoidBottomPadding: false,
          // backgroundColor: Colors.transparent,
          body: Container(
            width:MediaQuery.of(context).size.width,
            height:MediaQuery.of(context).size.height,
            
            alignment: Alignment.bottomRight,
            decoration: BoxDecoration(
                image: DecorationImage( 
              colorFilter: new ColorFilter.mode(Colors.green.withOpacity(1.0), BlendMode.saturation),
                    image: AssetImage(
                        'assets/q.jpg'),
                   fit: BoxFit.none
             ) ), 
              
              child: Form(key:_formKey, autovalidateMode: AutovalidateMode.always,child: Center(
                child: (ListView(
                  
                  children: <Widget>[
                    Container( 
                      
                      width:MediaQuery.of(context).size.width*0.2,
                      height:MediaQuery.of(context).size.height*0.2,
                      alignment: Alignment.topCenter,
                      padding: EdgeInsets.fromLTRB(0,0,0,0),
                      decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage(
                                'assets/haxer.jpg'),
                            // fit: BoxFit.fitHeight,),
                          scale:3.0,
                          alignment:Alignment.topCenter
                        ),  ),
                    ),
                  //  SizedBox(height:10,),
                    Container(
                        width:MediaQuery.of(context).size.width*0.2,
                        height:MediaQuery.of(context).size.height*0.1,
                      
                        padding: EdgeInsets.fromLTRB(69,20,10,0),
                        child: Text('Welcome Again',
                            style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.w900,
                              fontSize: 30,
                              fontStyle: FontStyle.italic,
                              
                            ))),
                //           SizedBox(height:0,),
                    Container(
                      width:MediaQuery.of(context).size.width*0.2,
                      height:MediaQuery.of(context).size.height*0.1,
             //         alignment: Alignment.topCenter,
                      padding: EdgeInsets.only(left: 25,right: 25),
                      child: TextFormField(keyboardType: TextInputType.emailAddress,
                        autofocus:false,
                        controller: _emailController,
                        validator:validateEmail,
                        onSaved:(value)=> _email =value,
                    decoration: InputDecoration(
                     // border: OutlineInputBorder(
                      //    borderRadius:
                       //       const BorderRadius.all(Radius.circular(50)),
                       //   borderSide: const BorderSide(
                      
                          //),
                      labelText: 'Email',
                      labelStyle:TextStyle( fontWeight: FontWeight.bold,),
                   //   focusedBorder:UnderlineInputBorder(borderSide: BorderSide(color:Colors.black)
                      )
                      ),
                    
                  //   validator: (val) {
                  //     if (val.length == 0)
                  //       return "Please enter Email";
                  //     else if (!val.contains('@'))
                  //       return "Invalid Email";
                  //     else
                  //       return null;
                  //   },
                  // onSaved: (val) => _email = val,
                  ),
          //      SizedBox(width: 10,
         //       height:5),
                Container(
                    width:MediaQuery.of(context).size.width*0.2,
                    height:MediaQuery.of(context).size.height*0.1,
              //      alignment:Alignment.topCenter,
                    padding: EdgeInsets.only(left:25.0,right:25.0),
                    child: TextFormField(
                      autofocus:false,
                      obscureText: true,
                      controller: _passwordController,
                      validator: validatePassword,
                      onSaved:(value)=> _password=value,
                      decoration: InputDecoration(
                     //   border: OutlineInputBorder(
                      //      borderRadius: const BorderRadius.all(
                      //        Radius.circular(50),
                           
                      //      borderSide: const BorderSide(
                       //       color: Colors.red,
                       //     ),
                        labelText: 'Password',
                        labelStyle:
                            TextStyle( fontWeight: FontWeight.bold),
                      ),
                      // validator: (val) {
                      //   if (val.length == 0)
                      //     return "Please enter password";
                      //   else if (val.length <= 5)
                      //     return "Your password should be more then 6 char long";
                      //   else
                      //     return null;
                      // },
                    //  onSaved: (val) => _password = val,
                    )),
Container(
                    width:MediaQuery.of(context).size.width*0.2,
                    height:MediaQuery.of(context).size.height*0.1,
                    alignment:Alignment(1.0, -1.5),
                    padding: EdgeInsets.only(top:0,left:20.0),
                    child: FlatButton(
                      child: Text(
                        'Forget Password',
                        style: TextStyle(
                           color: Colors.redAccent[400],
                           fontWeight:FontWeight.bold,decoration:TextDecoration.underline,fontSize:20
                        ),
                      ),
                  //    color: Colors.tealAccent,
                   //   shape: RoundedRectangleBorder(
                    //      borderRadius: BorderRadius.circular(100)),
          
     //     side: BorderSide(
                      //       color: Colors.redAccent[400],
                      //       width: 2.5,
                      //     )),
                      onPressed: () {

                        Navigator.push(context,MaterialPageRoute(builder:(context){
                            return ForgetPassword();}));
                        var nameController;
                        print(nameController.text);
                        var passwordController;
                        print(passwordController.text);
                      },
                      textColor: Colors.black54,
                      padding: EdgeInsets.all(8),
                       )
                       ),


                Container(

                  width:MediaQuery.of(context).size.width*0.2,
                   height:MediaQuery.of(context).size.height*0.071,
                  alignment: Alignment.bottomCenter,
                //   alignment: Alignment.topCenter,
                  
                  child: RaisedButton(
                    padding:EdgeInsets.fromLTRB(45,10,45,10),
                    child: Text(' Login  ',
                        style: TextStyle(                          color: Colors.black,
                          fontSize:25,
                        )),
                    color: Colors.tealAccent,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(100)),
                        // side: BorderSide(
                        //   color: Colors.redAccent[400],
                        //   width: 3.5,
                        // )),
                    onPressed: () {
                       if (_formKey.currentState.validate()) {
                       // Navigator.push(context,MaterialPageRoute(builder:(context){
                         //   return SecondRoute();}));
                         }else{
                         setState(() {
                           autovalidateMode=true;
                         });

                       }
                    //   //   _formKey.currentState.save();
                    //    _scaffoldKey.currentState.showSnackBar(SnackBar(
                    //      content: Text(
                    //     "Your email: $_email and Password: $_password"),
                    //    ));
                    //    //  }
                    //     if (_emailController.text.length == 0)
                    //     print( "Please enter Email");
                    //   else if (!_emailController.text.contains('@'))
                    //     print( "Invalid Email");
                    //     else if (_passwordController.text.length == 0)
                    //       print( "Please enter password");
                    //     else if (_passwordController.text.length <= 5)
                    //       print( "Your password should be more then 6 char long");
                    //     else
                    //       print('ok');
                    // },
                    // textColor: Colors.red,
                    // padding: EdgeInsets.fromLTRB(0, 0, 0, 5),
                    // visualDensity: VisualDensity.adaptivePlatformDensity,
                    }),
                ),
            //    SizedBox(
         //         height: 5,
               // );
                
                SizedBox(
                  height: 0,
                ),
                Container(
                   width:MediaQuery.of(context).size.width*0.2,
                   height:MediaQuery.of(context).size.height*0.071,
                  alignment: Alignment.bottomCenter,
             //    padding:EdgeInsets.fromLTRB(0,0,0,0),
                       child: RaisedButton(
                      padding:EdgeInsets.fromLTRB(45,10,45,10),
                      child: Text(
                        'Sign Up',
                        style: TextStyle(
                          fontSize: 25,
                          color: Colors.black,
                        ),
                      ),
                      color: Colors.tealAccent,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(100)),
                          // side: BorderSide(
                          //   color: Colors.purple[900],
                          //   width: 3.5,
                          // )),
                      onPressed: () {
Navigator.push(context,MaterialPageRoute(builder:(context){
                            return App();}));


                        //Click Here
                      },
                    ),
                ),

                  /*  Container(
                   width:MediaQuery.of(context).size.width*0.2,
                   height:MediaQuery.of(context).size.height*0.071,
                  alignment: Alignment.bottomCenter,
             //    padding:EdgeInsets.fromLTRB(0,0,0,0),
                       child: RaisedButton(
                      padding:EdgeInsets.fromLTRB(45,10,45,10),
                      child: Text(
                        'Otp Login',
                        style: TextStyle(
                          fontSize: 25,
                          color: Colors.black,
                        ),
                      ),
                      color: Colors.tealAccent,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(100)),
                          // side: BorderSide(
                          //   color: Colors.purple[900],
                          //   width: 3.5,
                          // )),
                      onPressed: () {



                        //Click Here
                      },
                    ), 
                
                )*/
            ],
                )
            ),  ),
              ),
        ),
      );          
  }


String validateEmail(String value) {
  Pattern pattern =
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
  RegExp regex = new RegExp(pattern);
  if (value.length == 0) {
    return "Email is Required";
  } else if (!regex.hasMatch(value))
    return 'Enter Valid Email';
  else
    return null;
}

String validatePassword(String value) {
  Pattern pattern = r'^(?=.*?[a-z])(?=.*?[0-9]).{8,}$';
  RegExp regex = new RegExp(pattern);
  if (value.length == 0) {
    return "Password is Required";
  } else if (!regex.hasMatch(value))
    return 'Password required: Alphabet, Number & 8 chars';
  else
    return null;
}




}