import 'package:flutter/material.dart';

import 'main.dart';
void main() => runApp(MaterialApp(
      home:MyApp(),
      debugShowCheckedModeBanner: false,
    ));

 class ForgetPassword extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

                       
    return Scaffold(
      body: Container(
            width:MediaQuery.of(context).size.width,
            height:MediaQuery.of(context).size.height,
            alignment: Alignment.bottomRight,
            decoration: BoxDecoration(
                image: DecorationImage( 
              colorFilter: new ColorFilter.mode(Colors.redAccent[100].withOpacity(0.9), BlendMode.color),
                    image: AssetImage(
                        'assets/q.jpg'),
                   fit: BoxFit.fill,
      ),),
         child:
        Center(  child:ListView( 
          children:<Widget>[
         
         
          Container(
           alignment:Alignment.topLeft,
           padding:EdgeInsets.fromLTRB(20,200,20,0),
            child:TextFormField(decoration: InputDecoration(labelText: "Enter Your Name",labelStyle: TextStyle(fontWeight: FontWeight.bold)),)
         ),
         Container( 
           alignment:Alignment.bottomLeft,
           padding:EdgeInsets.fromLTRB(20,10,20,0),
            child:TextFormField(decoration: InputDecoration(labelText: "Registered Phone Number",labelStyle: TextStyle(fontWeight: FontWeight.bold)),)
         ),
         Container( 
           alignment:Alignment.bottomLeft,
           padding:EdgeInsets.fromLTRB(20,10,20,0),
            child:TextFormField(decoration: InputDecoration(labelText: "Registered Email Id ",labelStyle: TextStyle(fontWeight: FontWeight.bold)),)
         ),
         Container( 
           alignment:Alignment.bottomLeft,
           padding:EdgeInsets.fromLTRB(20,10,20,50),
            child:TextFormField(decoration: InputDecoration(labelText: "Enter Your Date Of Birth",labelStyle: TextStyle(fontWeight: FontWeight.bold)),)
         ),
         Container(
                  width:MediaQuery.of(context).size.width*0.2,
                  height:MediaQuery.of(context).size.height*0.1,
                  padding: EdgeInsets.fromLTRB(90.0,0,90.0,30.0),
            color:Colors.transparent, 
           child:RaisedButton(
           padding:EdgeInsets.fromLTRB(50,15,50,15),
           
           child:Text('Submit',),
           
            color: Colors.transparent,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(100)),
              onPressed:(){}
           )
         ),
            ],  ), ),
          ),  );
  }
 }